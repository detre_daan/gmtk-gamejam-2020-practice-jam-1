﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyFront : MonoBehaviour
{

    AiPäth aiPäth;
    Transform parent;
    [SerializeField] float jumpPowah = 1f;
    private void Start()
    {
        parent = transform.parent;
        aiPäth = parent.GetComponent<AiPäth>();    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            Vector2 jumpVector = new Vector2(jumpPowah, 2);

             parent.GetComponent<Rigidbody2D>().AddForce(jumpVector, ForceMode2D.Impulse);
        }
    }
}

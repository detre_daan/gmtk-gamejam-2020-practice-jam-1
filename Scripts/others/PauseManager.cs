﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    public bool isPaused;
    public GameObject pauseMenu;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused) {
            Pause();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isPaused) {
            Resume();
        }
    }
    public void Pause() {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        isPaused = true;
    }

    public void Resume() {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        isPaused = false;
    }
}

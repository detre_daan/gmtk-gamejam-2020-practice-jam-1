﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamageOnSlam : MonoBehaviour
{
    bool alreadyAttacked = false;
    [SerializeField] int damageAmount = 10;
    private void OnTriggerStay2D(Collider2D other)
    {
        if (!other.CompareTag("Ground") && !other.CompareTag("Enemy")) {
            if (transform.parent.GetComponent<AiPäth>().attacking && !alreadyAttacked)
            {
                alreadyAttacked = true;
                PlayerMain player = other.gameObject.GetComponent<PlayerMain>();
                player.DealDamage(damageAmount);
            }
        }
    }


    private void Update()
    {
        if (!transform.parent.GetComponent<AiPäth>().attacking)
        {
            alreadyAttacked = false;
        }
    }
}

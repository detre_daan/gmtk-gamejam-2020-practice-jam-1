﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AiPäth : MonoBehaviour
{
    #region Variables
    [SerializeField] Transform target = null;
    [SerializeField] float speed = 25f;
    [SerializeField] float nextWaypointDistance = 1.3f;
    [SerializeField] Transform enemyGraphics = null;
    [SerializeField] float attackDistance = 5f;

    [Header("JumpyEnemy")]
    [SerializeField] bool jumpy = false;

    [SerializeField] bool inSky = false;
    [SerializeField] float minJumpPower = 5f;
    [SerializeField] float maxJumpPower = 15f;
    [SerializeField] int jumpChanceMax = 10;

    [Header("fatty")]
    [SerializeField] bool fatty = false;
    public bool attacking = false;
    [SerializeField] float attackMoveDistance = 1f;
    [SerializeField] float attackCooldownBase = 1f;
    [SerializeField] GameObject slamEffect = null;
    [SerializeField] Transform slamPos;
    [SerializeField] float particleCooldown;
    float attackCooldown;

    [Header("Normal enemy")]
    [SerializeField] bool normalEnemy = true;
    public float jumpPower = 5f;

    public Vector2 crownLocationOffset = new Vector2(0,0);

    int currentWaypoint = 0;

    Audio audio;
    Path path;
    Seeker seeker;
    Rigidbody2D rb;
    Animator anim;
    #endregion

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer.ToString() == "Floor")
        {
            inSky = false;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer.ToString() == "Floor")
        {
            inSky = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audio = FindObjectOfType<Audio>().GetComponent<Audio>();
        attackCooldown = attackCooldownBase;
        anim = GetComponent<Animator>();
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        //Repeat function, start cooldown, repeatrate
        InvokeRepeating("UpdatePath", 0f, .5f);
    }

    void UpdatePath()
    {

        if (target != null &&
            Vector3.Distance(target.position, transform.position) < attackDistance && !attacking) 
        {
            anim.SetBool("walking", true);
            if (seeker.IsDone())
            {
                //Creates path for with checkpoints for the enemy to move towards
                seeker.StartPath(rb.position, target.position, OnPathComplete);
            }

        }
        else
        {
            anim.SetBool("walking", false);
        }

    }

    void OnPathComplete(Path p)
    {
        //gives error when p can't be completed
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private void Update()
    {
        if (attacking)
        {
            attackCooldown -= Time.deltaTime;
            if (attackCooldown <= 0)
            {
                attacking = false;
                attackCooldown = attackCooldownBase;
            }
        }
    }

    void FixedUpdate()
    {
        if (!attacking)
        {
            MoveEnemyTonextPoint();
        }
    }

    //Looks for the next point to move towards and pushes enemy towards next point
    private void MoveEnemyTonextPoint()
    {
        if (path == null)
            return;

        //Checks if there are more waypoints in the path (end of path)
        if (currentWaypoint >= path.vectorPath.Count)
        {
            return;
        }

        // Takes position of current waypoint minus the current position giving a vector from
        // your position to the next waypoint and normalize it to ensure the length is 1
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        RotateEnemyToDirection();

        //move to the next waypoint if the waypoint is reached
        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;

            Random.InitState(System.DateTime.Now.Millisecond);
            float randomJump = Random.Range(0, jumpChanceMax);



            if (jumpy && randomJump == 0 && !inSky)
            {
                anim.SetTrigger("attack");
                Random.InitState(System.DateTime.Now.Millisecond);
                float jumpValue = Random.Range(minJumpPower, maxJumpPower);

                Vector2 jumpVector = new Vector2(1f, jumpValue);

                rb.AddForce(jumpVector, ForceMode2D.Impulse);
                inSky = true;

            }


            else if (fatty && 
            Vector3.Distance(target.position, transform.position) < attackMoveDistance)
            {
                attacking = true;
                anim.SetTrigger("attack");
                anim.SetBool("walking", false);            
            }
        }

    }

    public void FatBoyStep()
    {
            if (fatty)
            {
                audio.PlayFatStep();
            }                
    }
    public void FatBoyAttack()
    {
            GameObject slam = Instantiate(slamEffect, slamPos);
            audio.PlaySmash();
            Destroy(slam, 1f);        
    }

    //Rotate enemy in direction of force vector
    private void RotateEnemyToDirection()
    {
        if (rb.velocity.x >= 0.01f)
            enemyGraphics.localScale = new Vector3(-1, 1, 1);
        else if (rb.velocity.x <= -0.01f)
            enemyGraphics.localScale = new Vector3(1, 1, 1);
    }
}

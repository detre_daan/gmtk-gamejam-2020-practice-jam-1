﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMain : MonoBehaviour
{
    [SerializeField] float jumpForwardDistance = 2f;
    [SerializeField] float jumpForce = 5f;

    [SerializeField] float moveSpeed = 7.5f;
    [SerializeField] bool isGrounded = true;
    [SerializeField] bool isCrown = true;
    [SerializeField] bool isFatty = false;
    [SerializeField] bool isJumpy = false;
    [SerializeField] Animator anim;

    [Range(0, 100)]
    [SerializeField] float Health;
    [SerializeField] float groundedRaycast = .1f;
    Rigidbody2D rb;
    Vector3 jump;
    int floorLayerMask;
    bool isWalking = false;

    private void Start()
    {
        floorLayerMask = LayerMask.GetMask("Floor");
        rb = GetComponent<Rigidbody2D>();
        jump = new Vector2(jumpForwardDistance, jumpForce);
        anim = GetComponentInChildren<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Enemy")) {
            CrownAttach(collision);
        }
    }
    void FixedUpdate() {
        isGrounded = IsGrounded();
        if (isCrown) {
            CrownMovement();
        }
        else if (isFatty) {
            FattyMovement();
        }
        if (isCrown){
            anim.SetBool("IsWalking", isWalking);
        }
        if (Health <= 0 && isCrown) {
            anim.SetBool("IsDead", true);
        }        
    }
    void CrownMovement() {
        transform.Translate(Input.GetAxis("Horizontal") * moveSpeed * Time.fixedDeltaTime, 0f, 0f);


        Vector3 charScale = transform.localScale;
        if (Input.GetAxis("Horizontal") < 0) {
            isWalking = true;
            charScale.x = -1;
            jump = new Vector2(-jumpForwardDistance, jumpForce);

        }
        else if (Input.GetAxis("Horizontal") > 0) {
            isWalking = true;
            charScale.x = 1;
            jump = new Vector2(jumpForwardDistance, jumpForce);

        }
        else {
            isWalking = false;
        }
        if (isGrounded && Input.GetKey(KeyCode.Space)) {
            rb.AddForce(jump, ForceMode2D.Impulse);
        }

        transform.localScale = charScale;
        
    }
    void FattyMovement() {
        transform.Translate(Input.GetAxis("Horizontal") * moveSpeed * Time.fixedDeltaTime, 0f, 0f);

        Vector3 charScale = transform.localScale;
        if (Input.GetAxis("Horizontal") < 0) {
            charScale.x = 1;
            jump = new Vector2(-jumpForwardDistance, jumpForce);

        }
        else if (Input.GetAxis("Horizontal") > 0) {
            charScale.x = -1;
            jump = new Vector2(jumpForwardDistance, jumpForce);

        }
        //if (Input.GetKeyDown(KeyCode.Space) && isGrounded) {
        //    rb.AddForce(jump, ForceMode2D.Impulse);
        //    isGrounded = false;
        //}

        if (Input.GetButton("Fire1")) {

        }

        transform.localScale = charScale;
    }
    void CrownAttach(in Collider2D collision) {
        transform.SetParent(collision.transform);
        transform.position = collision.transform.position + new Vector3(collision.GetComponent<AiPäth>().crownLocationOffset.x, collision.GetComponent<AiPäth>().crownLocationOffset.y);
        collision.gameObject.GetComponent<AiPäth>().enabled = false;
        collision.gameObject.GetComponent<PlayerMain>().enabled = true;
        gameObject.GetComponent<PlayerMain>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().simulated = false;
        isCrown = false;
        isWalking = false;
        anim.SetBool("IsWalking", false);
    }
    public void DealDamage(int damageAmount) {
        Health -= damageAmount;
        if (Health == 0) {
            Debug.Log("GameOver!");
        }
    }
    bool IsGrounded() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector3.up, groundedRaycast, floorLayerMask);
        if (hit) {
            return true;
        }
        return false;
    }
    public void Die() {
        gameObject.SetActive(false);
    }
}

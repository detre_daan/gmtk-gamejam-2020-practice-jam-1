﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    public void Die() {
        if (GetComponentInParent<PlayerMain>() != null) {
            GetComponentInParent<PlayerMain>().Die();
        }
    }
}

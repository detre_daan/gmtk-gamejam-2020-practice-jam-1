﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    [SerializeField] float volume = 0.5f;

    [SerializeField] AudioClip smash;
    [SerializeField] AudioClip fatStep;
    public void PlaySmash()
    {
        PlaySound(smash);
    }

    public void PlayFatStep()
    {
        PlaySound(fatStep);
    }

    void PlaySound(AudioClip sound)
    {
        
        AudioSource.PlayClipAtPoint(sound, Camera.main.transform.position, volume);

    }
}
